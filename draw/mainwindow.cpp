#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <algorithm>
#include <QDebug>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <iostream>
#include <QTimer>
#include <bitset>
#include <QMouseEvent>
#include <QTime>
#include <QPainter>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("Drawbot");
    m_serialPortSensor = new QSerialPort(this);
    qDebug() << "Number of serial ports:" << QSerialPortInfo::availablePorts().count();
    ui->comboBox_portsName->clear();
    foreach (const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts())
    {
        qDebug() << "\nPort:" << serialPortInfo.portName();
        ui->comboBox_portsName->addItem(serialPortInfo.portName());
    }

    this->setMouseTracking(true);
    ui->centralWidget->setMouseTracking(true);
    ui->panel->setGeometry(rectanglePanel);

    rectangleDraw.setY(rectanglePanel.width()+margin);
    rectangleDraw.setX(margin);
    rectangleDraw.setWidth(width);
    rectangleDraw.setHeight(drawHeight);

    auto windowSize = geometry();
    windowSize.setHeight(margin+panelHeight+margin+drawHeight+margin);
    windowSize.setWidth(margin+width+margin);
    setGeometry(windowSize);

//    connect(&timeout, SIGNAL(timeout()), SLOT(handleTimeout()));
//    timeout.start(2000);
}

void MainWindow::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.setPen(Qt::blue);
    painter.drawRect(rectangleDraw);
}

MainWindow::~MainWindow()
{
    if (m_serialPortSensor->isOpen())
    {
        m_serialPortSensor->close();
        qDebug() << "...serial port is closed!";
    }
    delete ui;
}

void MainWindow::on_pushButton_portsRefresh_clicked()
{
    qDebug() << "Number of serial ports:" << QSerialPortInfo::availablePorts().count();
    foreach (const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts())
    {
        qDebug() << "\nPort:" << serialPortInfo.portName();
        ui->comboBox_portsName->clear();
        ui->comboBox_portsName->addItem(serialPortInfo.portName());
    }
}

void MainWindow::on_pushButton_portOpen_clicked()
{
    m_serialPortSensor->setPortName(ui->comboBox_portsName->currentText());

    m_serialPortSensor->setBaudRate(QSerialPort::Baud115200, QSerialPort::AllDirections);
    m_serialPortSensor->setDataBits(QSerialPort::Data8);
    m_serialPortSensor->setStopBits(QSerialPort::OneStop);
    m_serialPortSensor->setParity(QSerialPort::NoParity);
    m_serialPortSensor->setFlowControl(QSerialPort::SoftwareControl);
    connect(m_serialPortSensor, &QSerialPort::readyRead, this, &MainWindow::readyRead);
    qDebug() << "Start opening port...";
    m_serialPortSensor->open(QIODevice::ReadWrite);
    m_serialPortSensor->waitForReadyRead(10000);//dziala tylko z tym??

    if (m_serialPortSensor->isOpen())
    {
        qDebug() << "Serial port is open...";

    }
    else
    {
        qCritical() << "Couldn't open port: " << m_serialPortSensor->errorString();
    }
}

void MainWindow::on_pushButton_portClose_clicked()
{
    m_serialPortSensor->close();
    if (not m_serialPortSensor->isOpen())
    {
        qDebug() << "...serial port is closed!";
    }
}

void MainWindow::readyRead()
{
    isDone = true;
    QByteArray byteArray = m_serialPortSensor->readAll();
    if(byteArray.size() <= 0) return;
    QCoreApplication::processEvents(QEventLoop::AllEvents);
    qDebug() << "byteArray: " << QString(byteArray);
}

void MainWindow::mouseMoveEvent(QMouseEvent *mouseEvent)
{
    QPoint pos = mouseEvent->pos();

    if(rectangleDraw.contains(pos))
    {
        int deltaX = pos.x()-rectangleDraw.x();
        int deltaY = pos.y()-rectangleDraw.y();
        qDebug() << "mouseEvent inside rect x: " << deltaX << " y: " << deltaY;

        int axisZ = 0;
        if(mouseEvent->buttons() == Qt::LeftButton)
        {
            axisZ = 0;
        }
        else
        {
            axisZ = 1;
        }

        if(m_serialPortSensor->isOpen())
        {
            char symbols[21] = {};
            sprintf(symbols, "%d;%d;%d;\r\n", pos.x(), pos.y(), axisZ);

            qDebug() << "sent: " << symbols;
            m_serialPortSensor->write(symbols, sizeof(symbols));
        }
    }
}

void MainWindow::handleTimeout()
{
    qDebug() << "handleTimeout";
    if(m_serialPortSensor->isOpen())
    {
//        m_serialPortSensor->waitForReadyRead(1000);
        QByteArray byteArray = m_serialPortSensor->readAll();
        if(byteArray.size() <= 0) return;
        qDebug() << "handleTimeout: " << QString(byteArray);
    }
}

void MainWindow::on_pushButton_send_clicked()
{
    const char *dataToSend = ui->lineEdit_send->text().toStdString().c_str();
    if(m_serialPortSensor->isOpen())
    {
        qDebug() << "sent: " << dataToSend;
        m_serialPortSensor->write(dataToSend, sizeof(dataToSend));
    }
}
