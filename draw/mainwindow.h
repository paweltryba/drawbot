#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSerialPort>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_portClose_clicked();
    void on_pushButton_portsRefresh_clicked();
    void on_pushButton_portOpen_clicked();
    void readyRead();
    void mouseMoveEvent(QMouseEvent *);
//    bool event(QEvent *event);
    void handleTimeout();
    void on_pushButton_send_clicked();

private:
    Ui::MainWindow *ui;
    QSerialPort *m_serialPortSensor;
    bool isDone = true;
    void delay();
    void paintEvent(QPaintEvent *);
    int panelHeight = 300;
    int drawHeight = 300;
    int width = 600;
    int margin = 10;
    QRect rectanglePanel{margin, margin, panelHeight, width};
    QRect rectangleDraw;
    QTimer timeout;
};

#endif // MAINWINDOW_H
