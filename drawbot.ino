#include <Servo.h>
#include <math.h>



Servo serwomechanizm;
int dirPin_x = 8;
int dirPin_y = 7;

int stepPin_x = 9;
int stepPin_y = 10;

int interup_x = 3;
int interup_y = 2;

class Silnik
{
public:
  Silnik(int p_pinDir, int p_pinStep, int p_pinEndStop)
  {
    pinStep = p_pinStep;
    pinDir = p_pinDir;
    pinEndStop = p_pinEndStop;
    pinMode(pinDir, OUTPUT); //dir
    pinMode(pinStep, OUTPUT); //step
    digitalWrite(pinDir, LOW);
    pinMode(pinEndStop, INPUT);
    pinMode(pinEndStop, INPUT_PULLUP);
  }
  
  bool run()
  {
    long now = micros();
    if(targetPos <= 0) 
    {
      return true;
    }
    
    if(now - lastUpdate >= timeDelta)
    {
      singleStep();
      currentPos += dir;
      lastUpdate = now;
      targetPos--;
    }
    return false;
  }
  
  void setDir(bool dir)
  {
    digitalWrite(pinDir, dir);
  }

  void newPosition(long p_newPosition)
  {
    Serial.println("p_newPosition: " + String(p_newPosition) + " currentPos: " + String(currentPos));
    long delta = p_newPosition - currentPos;
    if(delta > 0)
    {
      dir = 1;
      setDir(HIGH);
    }
    else
    {
      dir = -1;
      setDir(LOW);
    }
    targetPos = abs(delta);
    timeDelta = 1000000.0 / targetPos;
    lastUpdate = 0;

    Serial.println("timeDelta: " + String(timeDelta) + " delta: " + String(delta) + " targetPos: " + String(targetPos));
  }

  long pos()
  {
    return currentPos;
  }
  
  void singleStep()
  {
      //PORTB |= 1 << pinStep;
      digitalWrite(pinStep, HIGH);
      delayMicroseconds(1);
      //PORTB &= ~(1 << pinStep);
      digitalWrite(pinStep, LOW);
  }

  bool goHome()
  {
    setDir(LOW);
    if(digitalRead(pinEndStop))
    {
      singleStep();
      return false;
    }
    else
    {
      currentPos = 0;
      return true;
    }
  }

  long currentPos{};
  long targetPos{};
  long lastUpdate{};
  double timeDelta{};
  int pinStep;
  int pinDir;
  int dir{};
  int pinEndStop;
};

Silnik silnikX(dirPin_x, stepPin_x, interup_x);
Silnik silnikY(dirPin_y, stepPin_y, interup_y);

void goToHomeXY()
{
  Serial.println("Start goToHomeXY");
  bool homeX = silnikX.goHome();
  bool homeY = silnikY.goHome();
  while(homeX != true || homeY != true)
  {
    homeX = silnikX.goHome();
    homeY = silnikY.goHome();
    delayMicroseconds(1000);
  }
  Serial.println("End goToHomeXY");
}

void setup() {
  Serial.begin(115200);
  serwomechanizm.attach(5);
  serwomechanizm.write(0);
}

void konwert(String steps, double &x, double &y, double &z)
{
  double xyz[3] = {};
  String temp{};
  int axis = 0;

  int len = steps.length();
  for (int i = 0; i < len; i++)
  {
    if (steps[i] != ';')
    {
      temp += steps[i];
    }
    else
    {
      xyz[axis++] = temp.toDouble();
      temp = "";
    }
  }
  x = xyz[0];
  y = xyz[1];
  z = xyz[2];

  Serial.println("x/y/z");
  Serial.println(x);
  Serial.println(y);
  Serial.println(z);
}

long convertToStepsNum(double angle)
{
  return angle * 3200 / 360.0 * 88 / 16;
}

struct Kroki
{
  Kroki(long p_stepsX = 0, long p_stepsY = 0)
  {
    stepsX = p_stepsX;
    stepsY = p_stepsY;
  }
  long stepsX;
  long stepsY;
};

bool goToPosXY(double x, double y)
{
  long a = 200;
  long b = 200;
  Serial.println("x: "+String(x) + " y: "+String(y));
  long p0 = x * x + y * y - a * a - b * b;
  long p1 = 2 * a * b;

//  Serial.println("x * x: "+String(x * x));
//  Serial.println("y * y: "+String(y * y));
//  Serial.println("a * a: "+String(a * a));
//  Serial.println("b * b: "+String(b * b));
//  Serial.println("2 * a * b: "+String(2 * a * b));
//  Serial.println("p0: "+String(p0));
//  Serial.println("p1: "+String(p1));

  double f2 = -acos(p0 / (double)p1);

//  Serial.println("raw rad f2: "+String(f2));

  double p2 = b * sin(f2);
//  Serial.println("p2: "+String(p2));

  double p3 = a + b * cos(f2);
//  Serial.println("p3: "+String(p3));

  double p4 = atan(y / (double)x);
//  Serial.println("p4: "+String(p4));

  double p5 = atan(p2 / (double)p3);
//  Serial.println("p5: "+String(p5));

  double f1 = (p4 - p5);

//  Serial.println("raw rad f1: "+String(f1));

  f1 = f1 * (180 / (double)M_PI);
  f2 = f2 * (180 / (double)M_PI)*-1;

//  Serial.println("raw deg f1: "+String(f1));
//  Serial.println("raw deg f2: "+String(f2))*-1;

  double katY = 90-f1;
  double katX = f2-180+45+katY;

//  Kroki wyniki;
  if(katY > 180 || katX > katY || (katY - katX > 100))
  {
    Serial.println("katY: "+String(katY) + " katX: "+String(katX));
//    return wyniki;
    return false;
  }
  
  long stepsX = convertToStepsNum(katX);
  long stepsY = convertToStepsNum(katY);
//  wyniki.stepsX = stepsX;
//  wyniki.stepsY = stepsY;

  silnikX.newPosition(stepsX);
  silnikY.newPosition(stepsY);

  bool osX = silnikX.run();
  bool osY = silnikY.run();
  while(osX != true || osY != true)
  {
    osY = silnikY.run();
    osX = silnikX.run();
  }

  Serial.println("silnikX.pos: " + String(silnikX.pos()) + " silnikY.pos: " + String(silnikY.pos()));

  return true;
}

void markerUp()
{
  serwomechanizm.write(0);
  delay(4);
}

void markerDown()
{
  serwomechanizm.write(30);
  delay(4);
}

double currentPosX = 0;
double currentPosY = 0;


void executePositionXY(double x, double y)
{
  const int division = 10;
  double deltaX = (x-currentPosX)/(double)division;
  double deltaY = (y-currentPosY)/(double)division;
  bool wynik = false;
  Serial.println("deltaX: " + String(deltaX) + " deltaY: " + String(deltaY));
  for(int i = 0; i < division; ++i)
  {
    currentPosX += deltaX;
    currentPosY += deltaY;
    wynik = goToPosXY(currentPosX, currentPosY);
    if(wynik == false)
    {
      Serial.println("Nie udalo sie currentPosX: " + String(currentPosX) + " currentPosY: " + String(currentPosY));
//      return;
    }
    
    Serial.println("Udało sie currentPosX: " + String(currentPosX) + " currentPosY: " + String(currentPosY));
  }
}

void loop()
{
  delay(2);
  Serial.print("test");
  String steps = Serial.readString();
  if (steps.length() > 0)
  {
    Serial.print("Echo: " + steps);
//    if(steps == "HOME")
//    {
//      goToHomeXY();
//    }
//    else
//    {
//      double x = 0;
//      double y = 0;
//      double z = 0;
//      konwert(steps, x, y, z);
//      if(z > 0)
//      {
//        markerUp();
//      }
//      else
//      {
//        markerDown();
//      }
//      executePositionXY(x, y);
//    }
  }
  
}
